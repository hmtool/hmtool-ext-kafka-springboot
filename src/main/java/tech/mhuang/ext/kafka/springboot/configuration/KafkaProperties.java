package tech.mhuang.ext.kafka.springboot.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.mhuang.ext.kafka.admin.bean.KafkaInfo;

/**
 * kafka配置信息
 *
 * @author mhuang
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = "mhuang.kafka")
@Data
public class KafkaProperties extends KafkaInfo {

    /**
     * open kafka property..
     * default is true
     */
    private boolean enable = true;
}
